# Tucknology Typora Themes

This repository has useful themes for the awesome markdown editor [Typora](www.typora.io).

### Installation
1. Clone this repo somewhere convenient. 
2. Copy the `tucknology.css` file to `~/Library/Application Support/abnerworks.Typora/themes/`